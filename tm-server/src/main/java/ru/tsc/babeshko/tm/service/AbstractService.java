package ru.tsc.babeshko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.api.repository.IRepository;
import ru.tsc.babeshko.tm.api.service.IService;
import ru.tsc.babeshko.tm.enumerated.Sort;
import ru.tsc.babeshko.tm.exception.entity.ModelNotFoundException;
import ru.tsc.babeshko.tm.exception.field.EmptyIdException;
import ru.tsc.babeshko.tm.exception.field.IncorrectIndexException;
import ru.tsc.babeshko.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final R repository;

    public AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        return repository.add(model);
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        return repository.add(models);
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        return repository.set(models);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort);
    }

    @NotNull
    @Override
    public M findOneById(@NotNull final String id) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Optional<M> model = Optional.ofNullable(repository.findOneById(id));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @NotNull
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        if (index < 0 || index >= repository.getSize()) throw new IncorrectIndexException();
        @NotNull final Optional<M> model = Optional.ofNullable(repository.findOneByIndex(index));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @NotNull
    @Override
    public M remove(@NotNull final M model) {
        return repository.remove(model);
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final M model = findOneById(id);
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        if (index < 0 || index >= repository.getSize()) throw new IncorrectIndexException();
        @NotNull final M model = findOneByIndex(index);
        return remove(model);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public long getSize() {
        return repository.getSize();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        if (id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public void removeAll(@NotNull final Collection<M> collection) {
        if (collection.isEmpty()) return;
        repository.removeAll(collection);
    }

}